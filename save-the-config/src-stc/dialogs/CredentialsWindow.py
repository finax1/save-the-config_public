import binascii
from io import StringIO

import fernet
import pandas as pd
from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QTableWidget, QPushButton, QTableWidgetItem, QMessageBox
from fernet import Fernet

from dialogs.information import show_error
from integrations.yamlIntegration import load_from_file


class CredentialsWindow(QMainWindow):
    def __init__(self, mainwindow):
        super(CredentialsWindow, self).__init__()
        uic.loadUi("src-stc/designer/CredentialsWindow.ui", self)
        self.show()
        self.setWindowTitle("Credentials")
        self.mainwindow = mainwindow
        table = self.findChild(QTableWidget, "tableWidget")
        addRowButton = self.findChild(QPushButton, "addRowButton")
        addRowButton.clicked.connect(lambda: table.setRowCount(table.rowCount() + 1))

        saveButton = self.findChild(QPushButton, "saveButton")
        with open(r"cred.csv", 'rb') as textfile:
            stri = textfile.read()

        if not stri == b"":
            try:
                readLines = StringIO(
                    Fernet(str.encode(load_from_file("env.yml")["encryption-salt"])).decrypt(
                        stri).decode("utf-8"))
                csv_var = pd.DataFrame(pd.read_csv(readLines))
            except fernet.InvalidToken:
                show_error("Invalid cred.csv. Continuing with empty Table.")
                csv_var = pd.DataFrame([['', '', '', '', '']])
            except binascii.Error:
                show_error("Invalid Encryption salt. Continuing with empty Table.")
                csv_var = pd.DataFrame([['', '', '', '', '']])
        else:
            csv_var = pd.DataFrame([['', '', '', '', '']])

        table.setColumnCount(len(csv_var.columns))
        table.setRowCount(len(csv_var.index))
        for i in range(len(csv_var.index)):
            for j in range(len(csv_var.columns)):
                table.setItem(i, j, QTableWidgetItem(str(csv_var.iloc[i, j])))
        table.setHorizontalHeaderLabels(['Host', 'Username', 'Password', 'Secret', 'Type'])
        table.resizeColumnsToContents()
        table.resizeRowsToContents()

        def save_clicked():
            csv_str = ""
            ask = False
            try:
                for row in range(table.rowCount()):
                    rowstr = ""
                    for col in range(table.columnCount()):
                        if table.item(row, col) and table.item(row, col).text().strip() != "" and table.item(row, col).text().strip().lower() != "nan":
                            rowstr += table.item(row, col).text().strip() + ","
                        else:
                            ask = True
                            break
                    else:
                        csv_str += rowstr[:-1] + "\n"
                    continue
                if ask:
                    reply = QMessageBox.question(self, 'Save?',
                                                 'Incomplete Rows are being dismissed. Do you want to continue?',
                                                 QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.No:
                        return
                csv_str = "Host,Username,Password,Secret,Type\n" + csv_str

                with open("cred.csv", "wb") as file:
                    try:
                        file.write(Fernet(str.encode(load_from_file("env.yml")["encryption-salt"])).encrypt(csv_str.encode("utf-8")))
                    except binascii.Error:
                        show_error("Invalid Encryption Salt. Pleasy specify correctly.")
                self.mainwindow.refresh_ui()
                self.close()
            except:
                show_error("Error saving devices.")
        saveButton.clicked.connect(save_clicked)
