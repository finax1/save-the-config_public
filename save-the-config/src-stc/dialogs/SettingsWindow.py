from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QListWidget, QLineEdit, QPushButton, QCheckBox, QTabWidget, QSpinBox, QGroupBox, \
    QListWidgetItem

from dialogs.information import show_error
from integrations.yamlIntegration import load_from_file, save_to_file


class SettingsWindow(QWidget):
    def __init__(self, mainwindow):
        super(SettingsWindow, self).__init__()
        uic.loadUi("src-stc/designer/SettingsWindow.ui", self)
        self.show()
        self.setWindowTitle("Settings")
        self.mainwindow = mainwindow
        self.router_listwidget = self.findChild(QListWidget, "router_listview")
        self.switch_listwidget = self.findChild(QListWidget, "switch_listview")
        self.filter_listwidget = self.findChild(QListWidget, "filter_listview")
        self.encryption_salt = self.findChild(QLineEdit, "encryption_salt")
        self.git_acc_token = self.findChild(QLineEdit, "gitAccToken_strg")
        self.filter_strg = self.findChild(QLineEdit, "addFilter_strg")
        self.command_strg = self.findChild(QLineEdit, "addLine_strg")
        self.git_repo = self.findChild(QLineEdit, "gitRepo_strg")
        self.alert_URL = self.findChild(QLineEdit, "alert_URL")
        self.data_directory = self.findChild(QLineEdit, "path")
        self.git_Branch = self.findChild(QLineEdit, "gitBranch_strg")
        self.command_delete_button = self.findChild(QPushButton, "command_delete_button")
        self.filter_delete_button = self.findChild(QPushButton, "filter_delete_button")
        self.add_filter_button = self.findChild(QPushButton, "newFilter_button")
        self.add_command_button = self.findChild(QPushButton, "newLine_button")
        self.save_button = self.findChild(QPushButton, "save_button")
        self.get_message_check = self.findChild(QCheckBox, "getmessage_check")
        self.auto_delete_check = self.findChild(QCheckBox, "autodelete_check")
        self.push_git_check = self.findChild(QCheckBox, "pushToGit_check")
        self.githubbox = self.findChild(QGroupBox, "PushToGit")
        self.auto_delete_num = self.findChild(QSpinBox, "autodelete_num")
        self.router_switch_tabwidget = self.findChild(QTabWidget, "tabWidget")

        self.command_delete_button.clicked.connect(lambda: self.delete_command())
        self.filter_delete_button.clicked.connect(lambda: self.delete_filter())
        self.add_filter_button.clicked.connect(lambda: self.add_filter())
        self.add_command_button.clicked.connect(lambda: self.add_command())
        self.save_button.clicked.connect(lambda: self.saveSettings())

        self.push_git_check.stateChanged.connect(lambda state: (self.git_Branch.setEnabled(state > 0),
                                                                self.git_repo.setEnabled(state > 0),
                                                                self.git_acc_token.setEnabled(state > 0)))
        self.auto_delete_check.stateChanged.connect(lambda state: self.auto_delete_num.setEnabled(state > 0))
        self.get_message_check.stateChanged.connect(lambda state: self.alert_URL.setEnabled(state > 0))

        self.settings = load_from_file("env.yml")

        self.encryption_salt.setText(self.settings["encryption-salt"])
        self.alert_URL.setText(self.settings["alert-commander-url"])
        self.git_Branch.setText(self.settings["git-branch"])
        self.git_repo.setText(self.settings["git-repo"])
        self.git_acc_token.setText(self.settings["git-access-token"])
        self.data_directory.setText(self.settings["data-directory"])

        self.get_message_check.setChecked(self.settings["message"])
        self.auto_delete_check.setChecked(self.settings["auto-delete"])
        self.push_git_check.setChecked(self.settings["push-to-git"])

        self.auto_delete_num.setValue(self.settings["auto-delete-num"])

        for i in self.settings["router-commands"]:
            self.router_listwidget.addItem(QListWidgetItem(i))

        for i in self.settings["switch-commands"]:
            self.switch_listwidget.addItem(QListWidgetItem(i))

        for i in self.settings["filter-commands"]:
            self.filter_listwidget.addItem(QListWidgetItem(i))

    def delete_filter(self):
        try:
            item = self.filter_listwidget.takeItem(self.filter_listwidget.currentRow())
            item = None
        except:
            show_error("Error deleting filter.")
        return

    def add_filter(self):
        new_filter = self.filter_strg.text()
        self.filter_listwidget.addItem(QListWidgetItem(new_filter))
        return

    def delete_command(self):
        curr_tab = self.router_switch_tabwidget.currentWidget().objectName()
        try:
            if curr_tab == "Switch":
                item = self.switch_listwidget.takeItem(self.switch_listwidget.currentRow())
                item = None
            elif curr_tab == "Router":
                item = self.router_listwidget.takeItem(self.router_listwidget.currentRow())
                item = None
        except:
            show_error("Error deleting command.")
        return

    def add_command(self):
        current_tab = self.router_switch_tabwidget.currentWidget().objectName()
        new_command = self.command_strg.text()
        if current_tab == "Switch":
            self.switch_listwidget.addItem(QListWidgetItem(new_command))
        elif current_tab == "Router":
            self.router_listwidget.addItem(QListWidgetItem(new_command))
        return

    def saveSettings(self):
        try:
            if self.encryption_salt.text():
                self.settings["encryption-salt"] = self.encryption_salt.text()
            if self.alert_URL.text():
                self.settings["alert-commander-url"] = self.alert_URL.text()
            if self.git_Branch.text():
                self.settings["git-branch"] = self.git_Branch.text()
            if self.git_repo.text():
                self.settings["git-repo"] = self.git_repo.text()
            if self.git_acc_token.text():
                self.settings["git-access-token"] = self.git_acc_token.text()
            if self.data_directory.text():
                self.settings["data-directory"] = self.data_directory.text()

            self.settings["message"] = self.get_message_check.isChecked()
            self.settings["auto-delete"] = self.auto_delete_check.isChecked()
            self.settings["push-to-git"] = self.push_git_check.isChecked()

            self.settings["auto-delete-num"] = self.auto_delete_num.value()

            switch_arr = []
            for item in range(0, self.switch_listwidget.count()):
                switch_arr.append(self.switch_listwidget.item(item).text())
            self.settings["switch-commands"] = switch_arr

            router_arr = []
            for item in range(0, self.router_listwidget.count()):
                router_arr.append(self.router_listwidget.item(item).text())
            self.settings["router-commands"] = router_arr

            filter_arr = []
            for item in range(0, self.filter_listwidget.count()):
                filter_arr.append(self.filter_listwidget.item(item).text())
            self.settings["filter-commands"] = filter_arr


        except:
            show_error("Error saving settings.")
            return

        save_to_file("env.yml", self.settings)
        self.mainwindow.refresh_ui()
        self.close()
