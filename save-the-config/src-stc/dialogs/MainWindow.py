import binascii
import os
import traceback
from io import StringIO

import fernet
import pandas as pd

from PyQt5 import uic
from PyQt5.QtCore import QThreadPool
from PyQt5.QtWidgets import QDialog, QPushButton, QToolButton, QListWidget, QListWidgetItem
from fernet import Fernet

from bin.github import push_to_github
from bin.postfetch import postfetch
from bin.sshconnection import sshconnection, fetch_configuration
from bin.handleConfig import write_config, get_config, get_config_count
from dialogs.FetchChooserWindow import FetchChooserWindow
from dialogs.ProgressWindow import ProgressWindow
from dialogs.ViewChooserWindow import ShowChooserWindow
from dialogs.CompareWindow import CompareWindow
from dialogs.CredentialsWindow import CredentialsWindow
from dialogs.SettingsWindow import SettingsWindow
from dialogs.information import show_error, show_info
from integrations.yamlIntegration import load_from_file


class MainWindow(QDialog):
    def __init__(self):
        super(MainWindow, self).__init__()
        uic.loadUi("src-stc/designer/MainWindow.ui", self)
        self.show()
        self.threadpool = QThreadPool()
        self.comparewindow = QDialog()
        self.fetch_comparewindow = QDialog()
        self.progresswindow = QDialog()
        self.postfetchobj = []

        self.fetch_button = self.findChild(QPushButton, "fetchConfig")
        self.fetch_all_button = self.findChild(QPushButton, "fetchAll")
        self.show_config_button = self.findChild(QPushButton, "showCurrent")
        self.compare_button = self.findChild(QPushButton, "compare")
        self.settings_button = self.findChild(QToolButton, "settingsButton")
        self.devices_button = self.findChild(QToolButton, "devicesButton")
        self.exit_button = self.findChild(QPushButton, "exit")
        self.list_widget = self.findChild(QListWidget, "geraeteList")

        self.exit_button.clicked.connect(lambda: self.button_exit_pressed())
        self.show_config_button.clicked.connect(lambda: self.button_showconfig_pressed())
        self.fetch_button.clicked.connect(lambda: self.button_fetch_pressed())
        self.compare_button.clicked.connect(lambda: self.button_compare_pressed())
        self.fetch_all_button.clicked.connect(lambda: self.button_fetchall_pressed())
        self.settings_button.clicked.connect(lambda: SettingsWindow(self))
        self.devices_button.clicked.connect(lambda: CredentialsWindow(self))

        self.settings = load_from_file("env.yml")

        salt = str.encode(self.settings["encryption-salt"])

        if not os.path.exists("cred.csv"):
            with open(r"cred.csv", 'w') as file:
                file.write("")

        with open(r"cred.csv", 'rb') as textfile:
            stri = textfile.read()

        if not stri == b"":
            try:
                read_lines = StringIO(Fernet(salt).decrypt(stri).decode("utf-8"))
                self.csv_var = pd.DataFrame(pd.read_csv(read_lines, dtype=str))
            except fernet.InvalidToken:
                show_error("Invalid cred.csv. Continuing with empty Table.")
                self.csv_var = pd.DataFrame(columns=['Host,Secret,User,Password,Type'], dtype=str)
            except binascii.Error:
                show_error("Invalid Encryption salt. Continuing with empty Table.")
                self.csv_var = pd.DataFrame(columns=['Host,Secret,User,Password,Type'], dtype=str)
        else:
            self.csv_var = pd.DataFrame(columns=['Host,Secret,User,Password,Type'], dtype=str)

        for index, row in self.csv_var.iterrows():
            self.list_widget.addItem(QListWidgetItem("(" + row['Type'] + ") " + row['Host']))

    def button_exit_pressed(self):
        self.close()

    def button_showconfig_pressed(self):
        if self.list_widget.currentRow() == -1:
            show_error("Please choose a device.")
            return
        act_row = self.csv_var.iloc[self.list_widget.currentRow()]
        try:
            data = load_from_file(self.settings['data-directory'] + "/" + act_row['Host'].replace(".", "-") + ".yml")
            dates = []
            for fetch in data['fetches']:
                dates.append(fetch['date'])

            ShowChooserWindow(dates, act_row['Host'], self.settings['data-directory'])
        except:
            show_error("No configurations found.")
            return

    def button_compare_pressed(self):
        if self.list_widget.currentRow() == -1:
            show_error("Please choose a device.")
            return
        act_row = self.csv_var.iloc[self.list_widget.currentRow()]
        try:
            self.comparewindow = CompareWindow(get_config(act_row['Host'], -2, self.settings['data-directory']),
                                               get_config(act_row['Host'], -1, self.settings['data-directory']),
                                               "Comparing Latest and Next-To-Latest fetched configuration of " +
                                               act_row['Host'])
        except:
            traceback.print_exc()
            show_error("Please fetch at least 2 versions.")
            return

    def show_errow_w(self, res):
        show_error(res)
        self.progresswindow = None

    def button_fetch_pressed(self):
        if self.list_widget.currentRow() == -1:
            show_error("Please choose a device.")
            return
        act_row = self.csv_var.iloc[self.list_widget.currentRow()]
        if act_row['Type'].startswith('R'):
            device_type = 'router-commands'
        elif act_row['Type'].startswith('S'):
            device_type = 'switch-commands'
        else:
            show_error("Invalid Type. Device can either be Router or Switch.")
            return
        try:
            p1 = sshconnection(act_row['Host'], act_row['Username'], act_row['Password'],
                               act_row['Secret'], self.settings[device_type],
                               self.settings['filter-commands'])
            self.threadpool.start(p1)
            self.progresswindow = ProgressWindow()
            self.progresswindow.set_text("Fetching and Saving Configuration from " + act_row['Host'])
            p1.signals.result.connect(lambda res: self.post_fetch_single(res, act_row))
            p1.signals.failed.connect(lambda res: self.show_errow_w(res)) # umständlich... leider bei python

        except:
            show_error("An error occured while fetching.")
            return

    def post_fetch_single(self, received_config, act_row):
        if self.settings['auto-delete']:
            auto_delete = self.settings['auto-delete-num']
        else:
            auto_delete = False

        base64_dict = {k: received_config[k] for k in received_config.keys() - {'configplain'}}
        plain_dict = {k: received_config[k] for k in received_config.keys() - {'config'}}

        write_config(act_row['Host'], base64_dict, auto_delete, self.settings['data-directory'])
        if self.settings['push-to-git']:
            if not self.settings['git-access-token'] or not self.settings['git-branch'] \
                    or not self.settings['git-repo']:
                show_error("git-settings not correctly specified")
            else:
                push_to_github(self.settings['git-access-token'], self.settings['git-repo'],
                               self.settings['git-branch'],
                               act_row['Host'], plain_dict)

        self.progresswindow = None
        if get_config_count(act_row['Host'], self.settings['data-directory']) > 1:
            self.fetch_comparewindow = CompareWindow(
                get_config(act_row['Host'], -2, self.settings['data-directory']),
                get_config(act_row['Host'], -1, self.settings['data-directory']),
                "Comparing Latest and Next-To-Latest fetched configuration of " +
                act_row['Host'])

        else:
            self.progresswindow = None
            show_info("First Configuration fetched. Fetch another to compare.")

    def start_post_fetch(self, row, res):
        self.postfetchobj.append(postfetch(row, res, self.settings))
        self.threadpool.start(self.postfetchobj[-1])
        self.postfetchobj[-1].signals.result.connect(lambda ress: (self.all_diffs.append(ress), print(ress)))

    def button_fetchall_pressed(self):
        if len(self.list_widget) == 0:
            show_info("No devices stored.")
            return
        all_diffs = []
        for index, row in self.csv_var.iterrows():
            if row['Type'].startswith('R'):
                device_type = 'router-commands'
            elif row['Type'].startswith('S'):
                device_type = 'switch-commands'
            else:
                show_error("Invalid Type. Device can either be Router or Switch.")
                return
            try:
                received_config = fetch_configuration(row['Host'], row['Username'], row['Password'],
                                                      row['Secret'], self.settings[device_type],
                                                      self.settings['filter-commands'])
            except:
                continue

            if self.settings['auto-delete']:
                auto_delete = self.settings['auto-delete-num']
            else:
                auto_delete = False

            base64_dict = {k: received_config[k] for k in received_config.keys() - {'configplain'}}
            plain_dict = {k: received_config[k] for k in received_config.keys() - {'config'}}

            write_config(row['Host'], base64_dict, auto_delete, self.settings['data-directory'])
            if self.settings['push-to-git']:
                if not self.settings['git-access-token'] or not self.settings['git-branch'] \
                        or not self.settings['git-repo']:
                    show_error("git-settings not correctly specified")
                else:
                    push_to_github(self.settings['git-access-token'], self.settings['git-repo'],
                                   self.settings['git-branch'],
                                   row['Host'], plain_dict)

            act_row = self.csv_var.loc[self.csv_var['Host'] == row['Host']].iloc[0]
            if get_config_count(act_row['Host'], self.settings['data-directory']) > 1:
                if get_config(act_row['Host'], -2, self.settings['data-directory']) != get_config(act_row['Host'], -1,
                                                                                                  self.settings[
                                                                                                      'data-directory']):
                    all_diffs.append(row['Host'])
        if len(all_diffs) == 0:
            show_info("No differences found.")
        else:
            FetchChooserWindow(all_diffs, self.settings['data-directory'])

    def refresh_ui(self):
        self.settings = load_from_file("env.yml")
        self.list_widget.clear()
        salt = str.encode(self.settings["encryption-salt"])
        if not os.path.exists("cred.csv"):
            with open(r"cred.csv", 'w') as file:
                file.write("")
        with open(r"cred.csv", 'rb') as textfile:
            stri = textfile.read()
        if not stri == b"":
            read_lines = StringIO(Fernet(salt).decrypt(stri).decode("utf-8"))
            self.csv_var = pd.DataFrame(pd.read_csv(read_lines, dtype=str))
        else:
            self.csv_var = pd.DataFrame(columns=['Host,Secret,User,Password,Type'])
        for index, row in self.csv_var.iterrows():
            self.list_widget.addItem(QListWidgetItem("(" + row['Type'] + ") " + row['Host']))
