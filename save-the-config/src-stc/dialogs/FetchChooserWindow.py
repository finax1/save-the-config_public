from io import StringIO
import pandas as pd

from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QListWidget, QPushButton, QListWidgetItem, QDialog
from fernet import Fernet

from bin.handleConfig import get_config
from dialogs.CompareWindow import CompareWindow
from dialogs.information import show_error
from integrations.yamlIntegration import load_from_file


class FetchChooserWindow(QWidget):
    def __init__(self, elms, path):
        super(FetchChooserWindow, self).__init__()
        uic.loadUi("src-stc/designer/ChooserWindow.ui", self)
        self.comparewindow = QDialog()
        self.show()
        self.setWindowTitle("Choose...")
        self.listwidget = self.findChild(QListWidget, "listWidget")
        self.pushButton = self.findChild(QPushButton, "pushButton")
        self.pushButton.clicked.connect(lambda: self.pushed())
        self.path = path
        for i in elms:
            self.listwidget.addItem(QListWidgetItem(i))

        with open(r"cred.csv", 'rb') as textfile:
            stri = textfile.read()
        read_lines = StringIO(Fernet(
                                str.encode(load_from_file("env.yml")["encryption-salt"]))
                                .decrypt(stri).decode("utf-8"))
        self.csv_var = pd.DataFrame(pd.read_csv(read_lines))

    def pushed(self):
        current_ip = self.listwidget.currentItem().text()
        act_row = self.csv_var.loc[self.csv_var['Host'] == current_ip]
        index = self.listwidget.currentRow()
        try:
            self.comparewindow = CompareWindow(get_config(act_row['Host'][0], -2, self.path), get_config(act_row['Host'][0], -1, self.path),
                          "Comparing Latest and Next-To-Latest fetched configuration of " + act_row['Host'][index])
        except:
            show_error("Error comparing Configurations. Please lookup manually.")
