from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QListWidget, QPushButton, QListWidgetItem, QDialog

from dialogs.ConfigurationWindow import ConfigurationWindow
from dialogs.information import show_error
from integrations.yamlIntegration import load_from_file


class ShowChooserWindow(QWidget):
    def __init__(self, elms, host, path):
        super(ShowChooserWindow, self).__init__()
        uic.loadUi("src-stc/designer/ChooserWindow.ui", self)
        self.configurationwindow = QDialog()
        self.show()
        self.setWindowTitle("Choose...")
        listwidget = self.findChild(QListWidget, "listWidget")
        push_button = self.findChild(QPushButton, "pushButton")
        for i in elms:
            listwidget.addItem(QListWidgetItem(i))

        def pushed():
            try:
                data = load_from_file(path + "/" + host.replace(".", "-") + ".yml")
                self.configurationwindow = ConfigurationWindow(data['fetches'][listwidget.currentRow()]['config'], "Configuration")
                self.close()
            except:
                show_error("Error opening configuration. Corrupt?")
                return

        push_button.clicked.connect(pushed)
