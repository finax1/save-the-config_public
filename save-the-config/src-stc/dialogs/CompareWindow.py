from difflib import unified_diff

from PyQt5 import uic
from PyQt5.QtWidgets import QDialog, QPlainTextEdit

from dialogs.information import show_info
from integrations.base64Integration import base64decode
from integrations.yamlIntegration import load_from_file


class CompareWindow(QDialog):
    def __init__(self, arr_previous, arr_current, title):
        super(CompareWindow, self).__init__()
        uic.loadUi("src-stc/designer/Compare.ui", self)
        self.show()
        self.setWindowTitle(title)
        text_field = self.findChild(QPlainTextEdit, "textEdit")
        text_current = ""
        text_previous = ""
        for elm in arr_previous.values():
            text_previous += base64decode(elm) + "\n"
        for elm in arr_current.values():
            text_current += base64decode(elm) + "\n"

        red_html = "<font color=\"red\">"
        green_html = "<font color=\"green\">"
        end_html = "</font>"

        for line in unified_diff(text_previous.split("\n"), text_current.split("\n"), 'Previous-Config',
                                 'Current-Config', lineterm='',
                                 n=load_from_file("env.yml")['diff-lines']):
            if line[0] == '-':
                text_field.appendHtml(red_html + line + end_html)
            elif line[0] == '+':
                text_field.appendHtml(green_html + line + end_html)
            else:
                text_field.appendHtml(line)
        if text_field.toPlainText() == "":
            self.close()
            show_info("No differences found.")