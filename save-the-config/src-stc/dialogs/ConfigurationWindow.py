from PyQt5 import uic
from PyQt5.QtWidgets import QWidget, QPlainTextEdit, QVBoxLayout, QLabel, QHBoxLayout, QScrollArea, QMainWindow
from PyQt5.QtCore import Qt

from integrations.base64Integration import base64decode


class ConfigurationWindow(QMainWindow):
    def __init__(self, data, title,):
        super(ConfigurationWindow, self).__init__()
        uic.loadUi("src-stc/designer/ConfigurationWindow.ui", self)
        self.show()
        self.setWindowTitle(title)

        try:
            scroll = QScrollArea()
            outerVbox = QVBoxLayout()
            widget = QWidget()

            scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
            scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            scroll.setWidgetResizable(True)
            scroll.setWidget(widget)

            for key, value in data.items():
                innerVbox = QVBoxLayout()
                label = QLabel(key, self)
                plainText = QPlainTextEdit()
                plainText.appendPlainText(base64decode(value))
                plainText.setFixedHeight(100)
                plainText.setFixedWidth(560)
                plainText.setReadOnly(True)

                innerVbox.addWidget(label)
                innerVbox.addWidget(plainText)

                outerVbox.addLayout(innerVbox)

            widget.setLayout(outerVbox)
            self.setCentralWidget(scroll)
        except Exception as i:
            print(i)