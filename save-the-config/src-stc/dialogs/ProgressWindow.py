from PyQt5 import uic
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLabel, QDialog


class ProgressWindow(QDialog):
    def __init__(self):
        super(ProgressWindow, self).__init__()
        uic.loadUi("src-stc/designer/ProgressWindow.ui", self)
        self.show()
        self.setWindowTitle("Fetching Configuration...")
        self.textlabel = self.findChild(QLabel, "textlabel")

    def set_text(self, text):
        self.textlabel.setText(text)

    def close(self):
        self.close()
