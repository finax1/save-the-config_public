from PyQt5.QtWidgets import QMessageBox


def show_error(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText("Error")
    msg.setInformativeText(text)
    msg.setWindowTitle("Error")
    msg.exec_()


def show_info(text):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Information)
    msg.setStyleSheet("QLabel{min-width: 100px;}")
    msg.setText("Info")
    msg.setInformativeText(text)
    msg.setWindowTitle("Info")
    msg.exec_()