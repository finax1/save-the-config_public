import traceback
from datetime import datetime

from github import Github, InputGitTreeElement, GithubException

from dialogs.information import show_error
from integrations.yamlIntegration import dict_to_yaml


def push_to_github(access_token, repo, branch, host, yaml):
    try:
        g = Github(access_token)
        repo = g.get_user().get_repo(repo)
        commit_message = "Fetch " + host + " from " + datetime.now().strftime(
            "%d/%m/%Y %H:%M:%S")
        master_ref = repo.get_git_ref(branch)
        master_sha = master_ref.object.sha
        base_tree = repo.get_git_tree(master_sha)
        element_list = list()
        element = InputGitTreeElement(host.replace(".", "-") + ".yml", '100644', 'blob',
                                      dict_to_yaml(yaml))
        element_list.append(element)
        tree = repo.create_git_tree(element_list, base_tree)
        parent = repo.get_git_commit(master_sha)
        commit = repo.create_git_commit(commit_message, tree, [parent])
        master_ref.edit(commit.sha)
    except GithubException as ge:
        show_error("Error pushing to github : %s" % ge)
    except Exception:
        show_error("Error pushing to github")
