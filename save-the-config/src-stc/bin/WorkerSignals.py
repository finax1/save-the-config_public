from PyQt5.QtCore import QObject, pyqtSignal


class WorkerSignals(QObject):
    result = pyqtSignal(object)
    failed = pyqtSignal(object)