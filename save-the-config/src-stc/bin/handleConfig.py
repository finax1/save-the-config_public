import os

from integrations.yamlIntegration import save_to_file, load_from_file


def write_config(host, config, auto_delete, path):
    if not os.path.exists(path):
        os.makedirs(path)
    filename = path + "/" + host.replace(".", "-") + ".yml"
    if not os.path.exists(filename):
        save_to_file(filename, {'fetches': [config]})
    else:
        cur_yaml = load_from_file(filename)
        if auto_delete:
            cur_yaml['fetches'] = cur_yaml['fetches'][-auto_delete + 1:]
        cur_yaml['fetches'].append(config)
        save_to_file(filename, cur_yaml)


def get_config(host, num, path):
    try:
        return load_from_file(path + "/" + host.replace(".", "-") + ".yml")['fetches'][num]['config']
    except IndexError:
        return ""


def get_config_count(host, path):
    return len(load_from_file(path + "/" + host.replace(".", "-") + ".yml")['fetches'])
