import re
from datetime import datetime

from PyQt5.QtCore import QRunnable, pyqtSlot

from bin.WorkerSignals import WorkerSignals
from dialogs.information import show_error
from integrations.base64Integration import base64encode
from integrations.sshIntegration import connection

class sshconnection(QRunnable):

    def __init__(self, host, username, password, secret, commands, filtercommands):
        super(sshconnection, self).__init__()
        self.username = username
        self.host = host
        self.password = password
        self.secret = secret
        self.commands = commands
        self.filtercommands = filtercommands
        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        c = connection(self.host, self.username, self.password, self.secret)
        try:
            c.connect()
            c.enable()
        except:
            self.signals.failed.emit("Error connecting to device %s" % self.host)
            return
        config_dict = {'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S"), 'config': {}, 'configplain': {}}
        for cmd in self.commands:
            try:
                stri = ""
                command_res = c.execute_command(cmd)
                for line in command_res.splitlines():
                    if not re.match("(" + ")|(".join(self.filtercommands) + ")",
                                    line) and not re.match("^/s*$", line):
                        stri += line + "\n"
                config_dict['config'][cmd] = base64encode(stri)
                config_dict['configplain'][cmd] = stri
            except:
                self.signals.failed.emit("Error executing command: " + cmd)
        try:
            c.disconnect()
        except:
            self.signals.failed.emit("Error closing connection. Configuration will be saved.")
        self.signals.result.emit(config_dict)


def fetch_configuration(host, username, password, secret, commands, filtercommands):
    c = connection(host, username, password, secret)
    try:
        c.connect()
        c.enable()
    except:
        show_error("Error connecting to device %s" % host)
        raise Exception
    config_dict = {'date': datetime.now().strftime("%d/%m/%Y %H:%M:%S"), 'config': {}, 'configplain': {}}
    for cmd in commands:
        try:
            stri = ""
            command_res = c.execute_command(cmd)
            for line in command_res.splitlines():
                if not re.match("(" + ")|(".join(filtercommands) + ")",
                                line) and not re.match("^/s*$", line):
                    stri += line + "\n"
            config_dict['config'][cmd] = base64encode(stri)
            config_dict['configplain'][cmd] = stri
        except:
            show_error("Error executing command: " + cmd)
    try:
        c.disconnect()
    except:
        show_error("Error closing connection. Configuration will be saved.")
    return config_dict

