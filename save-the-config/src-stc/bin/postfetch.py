from PyQt5.QtCore import pyqtSlot, QRunnable

from bin.WorkerSignals import WorkerSignals
from bin.github import push_to_github
from bin.handleConfig import write_config, get_config_count, get_config


class postfetch(QRunnable):

    def __init__(self, act_row, received_config, settings):
        super(postfetch, self).__init__()
        self.act_row = act_row
        self.received_config = received_config
        self.settings = settings
        self.all_diffs = []
        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        if self.settings['auto-delete']:
            auto_delete = self.settings['auto-delete-num']
        else:
            auto_delete = False

        base64_dict = {k: self.received_config[k] for k in self.received_config.keys() - {'configplain'}}
        plain_dict = {k: self.received_config[k] for k in self.received_config.keys() - {'config'}}

        write_config(self.act_row['Host'], base64_dict, auto_delete, self.settings['data-directory'])
        if self.settings['push-to-git']:
            if not self.settings['git-access-token'] or not self.settings['git-branch'] \
                    or not self.settings['git-repo']:
                self.signals.failed.emit("git-settings not correctly specified")
            else:
                push_to_github(self.settings['git-access-token'], self.settings['git-repo'],
                               self.settings['git-branch'],
                               self.act_row['Host'], plain_dict)

        if get_config_count(self.act_row['Host'], self.settings['data-directory']) > 1:
                if get_config(self.act_row['Host'], -2, self.settings['data-directory']) != get_config(self.act_row['Host'], -1,
                                                                                              self.settings[
                                                                                                  'data-directory']):
                    self.signals.result.emit(self.act_row['Host'])
                else:
                    self.signals.result.emit(None)
        else:
            self.signals.result.emit()
