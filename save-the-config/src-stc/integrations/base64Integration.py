import base64


def base64encode(stri):
    return base64.b64encode(stri.encode('ascii')).decode("ascii")


def base64decode(stri):
    return base64.b64decode(stri.encode('ascii')).decode('ascii')
