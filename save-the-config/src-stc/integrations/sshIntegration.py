from netmiko import ConnectHandler


class connection:

    def __init__(self, host, username, password, secret):
        self.net_connect = None
        self.connected = False
        self.cred = {
            "device_type": "cisco_ios",
            "host": host,
            "username": username,
            "password": password,
            "secret": secret,
        }

    def connect(self):
        if not self.connected:
            self.net_connect = ConnectHandler(**self.cred)
            self.connected = True
        else:
            raise Exception

    def disconnect(self):
        if self.connected:
            self.net_connect.disconnect()
            self.connected = False
        else:
            raise Exception

    def enable(self):
        if self.connected:
            self.net_connect.enable()
        else:
            raise Exception

    def execute_command(self, command):
        if self.connected:
            return self.net_connect.send_command(command)
        else:
            raise Exception

    def execute_command_set(self, set):
        if self.connected:
            return self.net_connect.send_config_set(set)
        else:
            raise Exception
