import yaml


def load_from_file(path):
    with open(path, "r") as textfile:
        return yaml.load(textfile, Loader=yaml.FullLoader)


def save_to_file(path, arr):
    with open(path, "w") as file:
        yaml.safe_dump(arr, file)


def dict_to_yaml(dict):
    return yaml.safe_dump(dict)
