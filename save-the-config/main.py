"""Add src-stc to PYTHONPATH"""
import os.path
import sys
sys.path.insert(0, os.path.dirname(__file__) + "/src-stc")

from PyQt5.QtWidgets import QApplication
from dialogs.MainWindow import MainWindow

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    app.exec_()
