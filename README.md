# Save The Config

A script fetching the configuration of cisco-Devices via ssh and providing a beautiful GUI.

## Requirements

## Setup development environemt

`git clone https://gitlab.com/finax1/save-the-config.git`

`cd save-the-config`

`pip install -r requirements.txt`

- You need to add save-the-config to your Python-Path. CD' into save-the-config and run:
  - `export PYTHONPATH=$PYTHONPATH:'cd ..; pwd'`

- In case the package `cryptograpthy` fails to install, download Microsoft C++ Tools here: https://visualstudio.microsoft.com/de/visual-cpp-build-tools/

- The newest version (1.19.4) of the package `numpy` has some problems with Windows. (https://github.com/numpy/numpy/pull/17547#issuecomment-714310892) 

    - Install version 1.19.3 instead: `pip install numpy==1.19.3`
  
### Run Program
`./save-the-config/main.py`