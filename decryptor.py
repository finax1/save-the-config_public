import sys
import yaml
from fernet import Fernet

with open("save-the-config/env.yml", "r") as textfile:
    settings = yaml.load(textfile, Loader=yaml.FullLoader)

data = sys.argv[1]
salt = str.encode(settings.get("encryption-salt"))

with open(data, 'rb') as textfile:
    str = textfile.read()

with open(data, 'wb') as textfile:
    textfile.write(Fernet(salt).decrypt(str))